#pragma once

#include <glib.h>
#include <stdint.h>
#include <math.h>

#define WIDTH         ( 1280 )
#define HEIGHT        ( 720 )
#define BPP           ( 3 )

#define VECY_MIN      ( 0.0 )
#define VECY_NORM     ( 2.032602071762 )

#define VECG_MIN      (-1.000000000000 )
#define VECG_MAX      ( 1.000000000000 )

#define VECB_MIN      (-0.889421641827 )
#define VECB_MAX      ( 0.992767393589 )

#define VECGMIN        (-1.000000000000 )
#define VECGNORM       ( 1.000000000000 - VECGMIN )

#define VECBMIN        (-0.889421641827 )
#define VECBNORM       ( 0.992767393589 - VECBMIN )

#define VECK_1 ( 0.8322445239 )
#define VECK_2 ( 0.8969499218 )
#define VECK_3 ( 0.1220425735 )
#define VECK_4 ( 0.1111949832 )
#define VECK_5 ( 0.0814066546 )
#define VECK_6 (-0.9925248663 )
#define VECK_7 ( 0.5133838359 )
#define VECK_8 (-0.4421321497 )
#define VECK_9 ( 0.1827465591 )

#define VECI_1 ( 0.5051870996 )
#define VECI_2 ( 0.2596220421 )
#define VECI_3 ( 1.0726713536 )
#define VECI_4 ( 0.6313989899 )
#define VECI_5 (-0.1065725655 )
#define VECI_6 (-1.0004756309 )
#define VECI_7 ( 0.1083845395 )
#define VECI_8 (-0.9871863977 )
#define VECI_9 ( 0.0381152153 )

#define U8_CLAMP(v) (uint8_t)(CLAMP((int)(v), 0, 255))

struct RGBpix {
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

struct WYZpix {
  uint8_t w;
  uint8_t y;
  uint8_t z;
};

void rgb2wyz(const struct RGBpix *rgb, struct WYZpix *wyz) {
    float EA = 0.0;
    float Es = 0.0;
    float Et = 0.0;
    float EC = 0.0;

    float EY = 0.0;
    float EG = 0.0;
    float EB = 0.0;
    float R = rgb->r / 255.0;
    float G = rgb->g / 255.0;
    float B = rgb->b / 255.0;

    EY = VECK_1 * R + VECK_2 * G + VECK_3 * B;
    EG = VECK_4 * R + VECK_5 * G + VECK_6 * B;
    EB = VECK_7 * R + VECK_8 * G + VECK_9 * B;

    EA = sqrt(EG * EG + EB * EB + EY * EY);
    EC = sqrt(EB * EB + EG * EG);

    Es = EC / EA;
    Es = ( EB < 0 ) ? -Es :  Es;

    Et = EG / EC;

    EY = EA;
    EG = Et;
    EB = Es;

    EY += -VECY_MIN;
    EG += -VECGMIN;
    EB += -VECBMIN;

    EY /= VECY_NORM;
    EG /= VECGNORM;
    EB /= VECBNORM;

    wyz->w = U8_CLAMP(EY * 255);
    wyz->y = U8_CLAMP(EG * 255);
    wyz->z = U8_CLAMP(EB * 255);
}

void wyz2rgb(const struct WYZpix *wyz, struct RGBpix *rgb) {
    float EY = wyz->w / 255.0;
    float EG = wyz->y / 255.0;
    float EB = wyz->z / 255.0;
    float EC = 0.0;
    float EA = 0.0;
    float Et = 0.0;
    float Es = 0.0;
    float EB_sign = 1.0;
    float EG_sign = 1.0;

    float R_, G_, B_;

    EY = (VECY_NORM * EY) + VECY_MIN;
    EG = (VECGNORM  * EG) + VECGMIN;
    EB = (VECBNORM  * EB) + VECBMIN;

    EB_sign = ( EB < 0 )? -1.0 : 1.0;
    EG_sign = ( EG < 0 )? -1.0 : 1.0;
    EA = EY;
    Et = fabs(EG);
    Es = fabs(EB);

    EC = Es * EA;
    EG = Et * EC;
    EB = EC * EC - EG * EG;
    EB = (EB <= 0)? 0.0 : sqrt(EB);
    EB *= EB_sign;
    EG *= EG_sign;

    EY = EA * EA - EB * EB - EG * EG;
    EY = ( EY < 0 ) ? 0 : sqrt(EY);

    R_ = VECI_1 * EY + VECI_2 * EG + VECI_3 * EB;
    G_ = VECI_4 * EY + VECI_5 * EG + VECI_6 * EB;
    B_ = VECI_7 * EY + VECI_8 * EG + VECI_9 * EB;

    rgb->r = U8_CLAMP(R_ * 255);
    rgb->g = U8_CLAMP(G_ * 255);
    rgb->b = U8_CLAMP(B_ * 255);
}

void convert_rgb2wyz(const struct RGBpix *src, struct WYZpix *dst, int width, int height) {
    for (int i = 0; i < width * height; ++i) {
        rgb2wyz(&src[i], &dst[i]);
    }
}

void convert_wyz2rgb(const struct WYZpix *src, struct RGBpix *dst, int width, int height) {
    for (int i = 0; i < width * height; ++i) {
        wyz2rgb(&src[i], &dst[i]);
    }
}