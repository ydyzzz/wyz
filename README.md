# GStreamer WYZ encoder/decoder filter

WYZ encoder/decoder plugin converts a frame from RGB color space to WYZ color space and vice versa.

## License

This code is provided under a MIT license [MIT], which basically means "do
with it as you wish, but don't blame us if it doesn't work". You can use
this code for any project as you wish, under any license as you wish. We
recommend the use of the LGPL [LGPL] license for applications and plugins,
given the minefield of patents the multimedia is nowadays. See our website
for details [Licensing].

## Building

```shell
meson build
ninja -C build
```

## Installation
```shell
sudo ninja -C build install
```

## Usage

Use X11 desktop as src and pass through the filters.
```shell
gst-launch-1.0 ximagesrc startx=0 starty=0 endx=1280 endy=720 use-damage=0 ! video/x-raw,framerate=30/1 ! \
videoscale method=0 ! videoconvert ! video/x-raw,width=1280,height=720,format=RGB ! \
wyzencoderfilter ! wyzdecoderfilter  ! autovideosink
```
Use v4l2 as src and pass through the filters.
```shell
gst-launch-1.0 v4l2src ! videoconvert ! "video/x-raw, format=RGB" ! wyzencoderfilter ! wyzdecoderfilter ! videoconvert ! autovideosink
```

Use v4l2 as src, pass though the encoder, motion jpeg, save to file and then use file as src, pass though motion jpeg decoder and the decoder filter
```shell
gst-launch-1.0 v4l2src ! videoconvert ! "video/x-raw, format=RGB" ! wyzencoderfilter ! jpegenc ! filesink location=test.mjp2
gst-launch-1.0 filesrc location=test.mjp2 ! jpegdec ! wyzdecoderfilter ! videoconvert ! autovideosink
```

[MIT]: http://www.opensource.org/licenses/mit-license.php or COPYING.MIT
[LGPL]: http://www.opensource.org/licenses/lgpl-license.php or COPYING.LIB
[Licensing]: https://gstreamer.freedesktop.org/documentation/application-development/appendix/licensing.html
